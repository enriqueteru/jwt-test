//EXPRESS
const express = require("express");
const server = express();

//CONFIG
const PORT = 3000;

//Morgan
var morgan = require("morgan");
server.use(morgan("combined"));


//INDEX ROUTES
const indexRouter = require("./routes/index.routes");
server.use("/", indexRouter);

//LOGIN ROUTES
const loginRoutes = require("./routes/login.routes");
server.use("/login", loginRoutes);

//HANDLER ERROR

server.all('*', function(req, res, next) {
    setImmediate(() => { next(new Error('Something bad happends')); });
  });
  
  server.use(function(error, req, res, next) {
    res.json({ message: error.message });
  });


  
//SERVER LISTEN
server.listen(PORT, () => {
  console.log(`running in http://localhost:${PORT}`);
});
