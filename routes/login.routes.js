var express = require("express");
var router = express.Router();
const jwt = require("jsonwebtoken");

//VISTA
router.get("/", (req, res, next) => {
  res.send("LOGIN");
});



//CREA TOKEN DE USER
router.post("/enter", (req, res, next) => {
  const user = {
    id: 1,
    name: "enrique",
    email: "Gat@gmail.com",
    password: "2335fdf4443",
  };

  jwt.sign({ user }, "secretkey", (err, token) => {
    res.json({ token });
  });
});


//FUNCION AUTENTIFICACIÓN
//Authorization: Bearer <token> ES UN METODO ASINCRONO ASYNC AWAIT NECESARIO
const verifyToken = async (req, res, next) => {
  const bearerHeader = await req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(403);
  }
};

//NECESITAS EL TOKEN PARA VER EL SECRETO
router.post("/secret", verifyToken, (req, res, next) => {
  jwt.verify(req.token, "secretkey", (error, authData) => {
    if (error) {
      res.sendStatus(403).json(error);
    } else {
      res.json({ created: true, authData });
    }
  });
});


module.exports = router;
